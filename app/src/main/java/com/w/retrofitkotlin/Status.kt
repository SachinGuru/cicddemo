package com.w.retrofitkotlin

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}