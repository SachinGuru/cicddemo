package com.w.retrofitkotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.ListView
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    private var superListView : ListView? = null
    private final val mCompositeDisposable: CompositeDisposable = CompositeDisposable();


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        superListView = findViewById(R.id.superListView)
        getSuperHeroes()
        //higher-order function declaration
//        fun higherfunc() : ((Int,Int)-> Int){
//            return ::mul
//        }
//
//        higherfunc().invoke(6,7)

    }

    public fun add(action:(a:Int,b:Int)-> Int){

    }

    public inline fun <T> Array<out T>.forEachInd(action: (index: Int, T) -> Unit): Unit {
        var index = 0
        for (item in this) action(index++, item)
    }



    // function declaration
    fun mul(a: Int, b: Int): Int{
        return a*b
    }
    private fun getSuperHeroes(){
        RetrofitClient.getMyApi()!!.getsuperHeroes()!!
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                val myheroList: List<Results>? = it as List<Results>
                val oneHeroes = arrayOfNulls<String>(myheroList!!.size)

                 fun update(i: Int,item: String?){
                    oneHeroes[i] = myheroList[i].superName
                }

                oneHeroes.forEachInd(::update)
//                oneHeroes.forEachInd{ index, s ->
//                    oneHeroes[index] = s
//                }
//                oneHeroes.forEachIndexed { index, s ->
//                    oneHeroes[index] = myheroList[index].superName
//                }
                superListView!!.adapter =
                    ArrayAdapter(applicationContext, android.R.layout.simple_list_item_1, oneHeroes)
            },{
                Log.e("Error","Message",it)
            })
//        RetrofitClient.getMyApi()!!.getsuperHeroes()!!.enqueue(object : Callback<List<Results?>?>{
//            override fun onResponse(
//                call: Call<List<Results?>?>,
//                response: Response<List<Results?>?>
//            ) {
//                val myheroList: List<Results>? = response.body() as List<Results>?
//                val oneHeroes = arrayOfNulls<String>(
//                    myheroList!!.size
//                )
//
//                for (i in myheroList!!.indices) {
//                    oneHeroes[i] = myheroList!![i].superName
//                }
//
//                superListView!!.adapter =
//                    ArrayAdapter(applicationContext, android.R.layout.simple_list_item_1, oneHeroes)
//            }
//
//            override fun onFailure(call: Call<List<Results?>?>, t: Throwable) {
//                TODO("Not yet implemented")
//            }
//
//        })

    }
}