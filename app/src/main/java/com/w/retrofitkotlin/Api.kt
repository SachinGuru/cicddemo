package com.w.retrofitkotlin

import io.reactivex.Observable
import retrofit2.Call
import retrofit2.http.GET

interface Api {
    companion  object {
        val BASE_URL: String = "https://simplifiedcoding.net/demos/"
    }

    @GET("marvel")
    fun getsuperHeroes(): Observable<List<Results?>?>?
}