package com.w.retrofitkotlin

import com.google.gson.annotations.SerializedName

data class Results(@SerializedName("name") val superName:String)
