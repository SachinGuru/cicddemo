package com.w.retrofitkotlin

data class User(
    val avatar: String,
    val email: String,
    val id: String,
    val name: String
)
